const express = require("express");

// Import validator
const {
  createOrUpdateGoodValidator,
} = require("../middlewares/validators/customers");

// Import controller
const {
  getAllCustomer,
  getDetailCustomer,
  createCustomer,
  updateCustomer,
  deleteCustomer,
} = require("../controllers/customers");

const router = express.Router();

router
  .route("/")
  .post(createOrUpdateGoodValidator, createCustomer)
  .get(getAllCustomer);

router
  .route("/:id")
  .get(getDetailCustomer)
  .put(createOrUpdateGoodValidator, updateCustomer)
  .delete(deleteCustomer);

module.exports = router;

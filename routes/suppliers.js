const express = require("express");

// Import validator
const {
  createSuppliersValidator,
} = require("../middlewares/validators/suppliers");

// Import controller
const { createSuppliers } = require("../controllers/suppliers");

const router = express.Router();

// router.route("/").post(createSuppliersValidator, createSuppliers);

module.exports = router;
